# Development on Bookworm

To work on a microscope, it's probably most convenient to set up SSH, proxy internet access, and public key authentication. I'll talk about two computers here: the *local* machine is the one you are using directly, usually your laptop. The *remote* machine is the Raspberry Pi in the microscope.

## Simple instructions

1. Generate an SSH key pair on your *local* computer, by running `ssh-keygen`. Skip this step if you already have a key at `~/.ssh/id_rsa.pub`. Enter a password to protect the key.
2. (optional but convenient) Add your SSH key to an authentication agent on your *local* computer (see below). If you don't do this, you'll need to enable login with a password on your *remote* Pi (see below).
3. Burn your microscope's SD card, using Raspberry Pi Imager. Click the gear button to configure the image, and make sure you:
    * Set the username
    * Set the hostname
    * Enable SSH access
    * Enable public key authentication
4. Connect to your microscope by running `ssh pi@microscope` on your *local* computer - this should use the public key you generated in (1) and should only ask for the password for your SSH key, that you set in (1).
8. On your *local* computer, obtain `sshterm` from Richard's `wot-updater` project. It's most easily found in the [github actions](https://github.com/rwb27/wot-updater/actions/). Unzip it and copy the relevant utility to a folder on your *local* computer.
9. Run `sshterm` with `./sshterm -user pi -hostname microscope` (substituting `pi` and `microscope` with the values you used for user and hostname). This should use your SSH key if it's added to an agent, otherwise it will ask for a password (if you enabled this, see below).
10. Start a separate SSH connection by running `ssh pi@microscope` from your *local* computer.
11. In this SSH connection, you should be able to run `export-wot-proxy` which will then give you temporary internet access on the *remote* Pi. See below for more information on using `pip` in a virtual environment with the proxy.
12. On the *remote* Pi, run `nano ~/.ssh/config` and add the line:
    ```
    ProxyCommand nc -v -x 127.0.0.1:10800 %h %p
    ```
13. On the *remote* Pi, run `ssh-keygen` to generate a public/private key pair on your *remote* machine. Alternatively, use SSH agent forwarding so that you authenticate with github/gitlab using the key stored on your *local* machine. This requires `ForwardAgent yes` in your *local* SSH config, next to the relevant host.
14. Set your git identity using `git config -g user.name "Your Name"` and `git config -g user.email "your@email.com"`
15. Update the remote address for your git repositories: `cd` into the folder, then run `git remote -v` to see the URL. You can set it to the SSH version with `git remote set-url origin git@gitlab.com:openflexure/openflexure-microscope-server.git` or similar.

### Logging in with a password

If you can't get the SSH agent working with `sshterm` for internet access on the *remote* Pi, you can also enable log in with a password:

1. Set a secure password for your *remote* Pi by running `passwd` in the SSH terminal. Only needed if you can't get the SSH agent to work (see 8).
2. On the *remote* Pi, run `sudo nano /etc/ssh/sshd_config`. There should be a line `PasswordAuthentication no`. Change this to `PasswordAuthentication yes`, or add that if there's no existing `PasswordAuthentication` line.
3. On the *remote* Pi, run `sudo systemctl restart sshd`, then disconnect your SSH terminal by typing `exit`.

## SSH authentication

`ssh` is more secure and more convenient if you use public key authentication. First, you'll need to generate a "key pair" on your *local* machine by running `ssh-keygen`. Do enter a password - otherwise you will be at risk if the key ever gets compromised. 

It is probably helpful to make sure you have a private key available via an ssh agent on your computer. Usually this is done by running `ssh-add` from a terminal, and entering your key's password (you did set one, right?). You may need to enable this first - there are various guides online for different operating systems. On Windows, if you have `openssh` installed via Windows, it should be as simple as running `Set-Service -Name sshd -StartupType 'Automatic'`. Other systems may not even require that, or may just need `eval ssh-agent -s`.

I recommend the "2019 update" answer in [this StackOverflow thread](https://stackoverflow.com/a/40720527). If you want to use SSH Agent Forwarding (to use the SSH key on your laptop from a Raspberry Pi you're developing on), you will need to update SSH on your *local* Windows machine to a prerelease version. This is available on the [official Github repo](https://github.com/PowerShell/Win32-OpenSSH/releases). I'm currently running 8.9.1.0, though anything more recent than that should work.

## SSH to the microscope

When you set up the image, you **must** set the username and password to something non-default. You can do this with the configuration button in the Raspberry Pi imager. It's strongly encouraged to use public key authentication (which you can also do via the Raspberry Pi imaging tool).

* Set the username
* Set the hostname
* Enable SSH access
* Enable public key authentication (should automatically grab your public key)

This will place the public key from your *local* machine into the `~/.ssh/authorized_keys` file on the *remote* machine. You can do this manually to add other public keys - just append to the file. The easiest way is to SSH in (e.g. using a password, or from a computer that already has access) and append it by running, on the *remote* machine, `cat >> ~/.ssh/authorized_keys`. You run that command, paste the contents of the public key, and then hit `Ctrl` + `D` to close the file. If you're only setting up one laptop, don't worry about this step.

### Enabling a password

Once you've burned the SD card and booted the Pi, you should be able to connect to it over an ethernet cable using `ssh user@hostname`, without a password. You may need to enable password log-in, which you can do from that SSH terminal on the *remote* machine. After setting a secure password with `passwd`, edit the SSH config with `sudo nano /etc/ssh/sshd_config`. Uncomment the line in `/etc/ssh/sshd_config` that reads `PasswordAuthentication yes`.

## Set up internet access

Internet access using the `sshterm` utility from my `wot-updater` project provides a proxy via your computer, without giving the microscope a routable internet connection. This is more secure than making a temporary hotspot/internet connection sharing, and less likely to violate acceptable use policies. Get the latest build of the utility from [github actions](https://github.com/rwb27/wot-updater/actions/), unzip it and copy the relevant utility to a folder on your *local* computer. You may wish to add it to your `PATH` if you know how. Mac users may need to run `xattr -d com.apple.quarantine sshterm-mac` to allow it to run. The latest version will try to use public key authentication via an SSH agent. See the first section for how to set that up.

You should now be able to connect using `./sshterm -username user -host raspberrypi` on your *local* machine. While that connection is open, you should be able to run `export-wot-proxy` from any SSH session on the *remote* Pi and get internet access from any program that supports the `http_proxy` environment variable. For programs that can't use it, prefixing the command with `wot-pc` or `sudo-wot-pc` will use `proxychains` to grant internet access to the command being run.

* In a Python virtual environment, you will need to run `wot-pc pip install pysocks` to enable proxy support. After that, `pip` should work normally.
* Git repositories are detailed later
* The terminal provided by `sshterm` is not as good as you get with `ssh`. I recommend connecting on `sshterm` to provide the proxy, but then making a separate `ssh` connection (or using a VSCode remote session) to actually run things.

## Set up git repositories

If you've run `export-wot-proxy`, running `git pull` on a repository cloned via `http` or `https` should work. Large file storage probably won't work: there's a command `skip-git-lfs` that will turn LFS off. You won't see the LFS files, but the rest of the repository will work. Pushing to an `https` repository may work, but you'll need to authenticate every time, which quickly gets tedious.

To enable push access, you should:

* SSH on to your Pi (with the `sshterm` connected)
* Run `ssh-keygen` to create a public/private key pair. You should set a password on this - otherwise there's a risk anyone with access to the Pi can abuse your GitHub/GitLab identity.
* In order to SSH out, you'll need to set an SSH proxy. The easiest way to do this is with a global configuration in `~/.ssh/config`. Edit this file with `nano ~/.ssh/config` and add a line that says 
```
ProxyCommand nc -v -x 127.0.0.1:10800 %h %p
```
* Add the public key to your GitHub/GitLab account(s). This is found in your user settings. To get the public key, run `cat ~/.ssh/id_rsa.pub` and copy/paste this into the web page.
* For each repository you want to push, you'll need to edit the remote to use SSH. `cd` into the repository, and print the remote address with `git remote -v`. You can then set the remote URL to use SSH instead. You can obtain the SSH url from the relevant repository page, but it's generally quite guessable based on the HTTP address:
```terminal
$ git remote -v
origin  https://gitlab.com/openflexure/openflexure-microscope-server.git (fetch)
origin  https://gitlab.com/openflexure/openflexure-microscope-server.git (push)
$ git remote set-url origin git@gitlab.com:openflexure/openflexure-microscope-server.git
```

Each time you want to do development, you should start by adding your public key to `ssh-agent` (this means you only need to type the password once):
```terminal
> eval `ssh-agent -s`
> ssh-add
```
After doing this, you should be able to `git push` and `git pull` without further drama. Note that you'll have to do the first step on each terminal you open - it's not shared system wide (for security reasons).

## Setting up a microscope for development

Once you can log in on SSH, the following should help get you set up to develop on the microscope. You'll need SSH agent forwarding enabled (see above), and you'll need to run it from a session with agent forwarding (i.e. from proper SSH, not `sshterm.exe`). The easiest way to run it is to type `cat > setup.sh` and then paste in the script below, hit `enter` then `Control` + `d`. You can then run it with `source setup.sh`.

```terminal
git config --global user.email "richard.bowman@cantab.net"
git config --global user.name "Richard Bowman"
cat > ~/.ssh/config <<< "ProxyCommand nc -v -x 127.0.0.1:10800 %h %p"
cd /var/openflexure/
sudo chown -R openflexure-ws:openflexure-ws .
sudo chmod -R g+w .
cd application
pushd openflexure-microscope-server
git config --global --add safe.directory `pwd`
git remote set-url origin git@gitlab.com:openflexure/openflexure-microscope-server.git
git pull
popd
pushd labthings-picamera2
git config --global --add safe.directory `pwd`
git remote set-url origin git@github.com:rwb27/labthings-picamera2.git
git pull
popd
pushd labthings-fastapi
git config --global --add safe.directory `pwd`
git remote set-url origin git@github.com:rwb27/labthings-fastapi.git
git pull
popd
pushd labthings-sangaboard
git config --global --add safe.directory `pwd`
git remote set-url origin git@github.com:rwb27/labthings-sangaboard.git
git pull
popd
pushd openflexure-stitching
git config --global --add safe.directory `pwd`
git remote set-url origin git@gitlab.com:openflexure/openflexure-stitching.git
git pull
popd
```
