# Python on Windows

If you want to run Python code on your laptop, you'll probaby need to install Python locally. It's possible to use various cloud solutions for this, but if you need to connect to a microscope that's not on the internet (which is most of them) you'll need to run code loccally. The method below is a bit "bare-bones" but it keeps things simple, and is often easier to debug.

> If you use Anaconda, or another similar "distribution", it's important to mention that if you ask anyone for help with your Python installation. Debugging this can be a huge waste of time.
{: .alert .alert-warning}

## Install Python

Richard's preferred way of installing Python is to download it directly from [python.org](https://www.python.org/downloads/). You should install the version appropriate to your system, which is probably the one ending `-amd64.exe`.

Click through the installer - I recommend ticking the option to "install for all users" (you'll need to customise the installation rather than just clicking "install now"), because this puts Python in a much more obvious place.

## Create a virtual environment

After installing Python, open a command prompt (I use PowerShell, and prefer to use it from Windows Terminal, which can be installed from the Windows Store). Navigate to your project folder, and create a virtual environment with:

```
py --version
py -m venv .venv
```

The first command should print your Python version (if it's less than 3.11, you may hit problems). 
If `py` is not recognised as a command, try `python`. 
The second command will create a folder called `.venv` in your project folder, which is your Python "virtual environment". This keeps a copy of Python, along with all the libraries you install. After installing it, you can "activate" the environment with:

```
.venv\Scripts\activate
```

On MacOS or Linux (including WSL) you will need to activate the environment using `source .venv/bin/activate` instead.
{: .alert .alert-info}

After activating the environment, you will see the prompt change, usually a `(.venv)` will appear in the prompt. Note that once you've activated the virtual environment, `python` should be recognised even if you only had `py` before. You can install packages now, and I usually start by updating `pip`:

```
python -m pip install --upgrade pip
```

Once that's done, install the OpenFlexure packages, and jupyter, with:

```
pip install openflexure-microscope-client numpy scipy matplotlib jupyter opencv-python notebook
```

After that's installed, you should be able to launch a notebook using

```
jupyter notebook .
```

Next time you need it, you should just have to activate the environment and run the notebook, i.e.

```
.venv/bin/activate
jupyter notebook .
```